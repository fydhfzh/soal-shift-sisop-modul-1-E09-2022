# Sistem Operasi - Modul 1

Praktikum sistem operasi modul 1

## Soal 1
Pada soal 1 ini, kami ditugaskan untuk membuat sebuah sistem register dan login yang dimana passwordnya memiliki ketentuan khusus dan setiap percobaan login maka hasil login akan tersimpan pada file log.txt dan setiap percobaan register akan tersimpan pada file user2.txt

### Poin A: Membuat file register dan disimpan pada user2.txt dan sistem login
membuat file script register dan login dimana sistem register disimpan pada user2.txt dengan
    
    echo "$uname" >> user2.txt
    echo "$pwd" >> user2.txt
    
### Poin B: Membuat Password dengan requirement
membuat password dengan requirement 
i.   Minimal 8 karakter
ii.  Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii. Alphanumeric
iv.  Tidak boleh sama dengan username
     
    while :
    do
    read -s pwd
    [ ${#pwd} -ge 8 ] && [ $pwd != $uname ] && [[ "$pwd" =~ [[:upper:]] ]] && [[ "$pwd" =~ [[:lower:]] ]] &&  [  [ $pwd =~ ^[a-zA-Z0-9]{3,20}$ ]] && break
    echo "Your password didn't meet the requirements, try again!"
    done
    

dengan diatas sebagai batasan agar saat password tidak sesuai requirement maka akan terprint "Your password didn't meet the requirements, try again!" dan  menggunakan read -s dikarenakan password input diharuskan untuk hidden

### Poin C: Menyimpan hasil login dan register ke log.txt
menyimpan hasil login dan register ke log.txt untuk tau siapa saja yang pernah login di sistem ini dan mengecek apakah username apa saja yang sudah ada namun tetap di register

### Poin C.1
mengecek pada user2.txt apakah username yang akan di register sudah terdaftar di sistem
    
    while :
    do
    read uname
    if grep -Fxq "$uname" user2.txt
    then
    echo 'Username already exists'
    echo '$TIMESTAMP REGISTER: ERROR User already exists' >> log.txt
    else
    break
    fi
    done
    
jadi ketika setelah di cek ternyata username sudah terdaftar maka akan tertulis pada log

### Poin C.2
memberi message pada log jika register telah berhasil dilakukan
    
    echo "Register is a success!"
    echo "$TIMESTAMP REGISTER: INFO User $uname registered successfully" >> log.txt
    
### Poin C.3
memberi message pada log jika terdapat user yang ingin login tetapi menginput password yang salah
    
    else    
    fail="Failed login attempt on user $username"
    echo $fail
    echo "$calendar $time LOGIN:ERROR $fail" >> $locLog 
    
 ketika password salah makan akan termessage "Failed login attempt on user $username" pada loclog dimana loclog sudah terdestinasi ke log.txt

### Poin C.4
memberi message pada log jika login telah berhasil dilakukan
    
    if [[ -n "$checkUser" ]] && [[ -n "$checkPass" ]]
    then
    echo "$calendar $time LOGIN:INFO User $username logged in" >> $locLog
    echo "Login success"
    
mengecek terlebih dahulu apakah user dan password telah terdaftar pada user2.txt dan jika sudah terdaftar maka
ketika login berhasil di lakukan maka akan di message ke loclog yang sudah terdestinasi ke log jika login telah berhasil dilakukan di waktu tertentu

### Poin D: Memberi command untuk download atau mengecek jumlah login
untuk mengecek jika command "dl" maka sistem akan mendownload dan jika command "att" maka akan mengecek log berapa kali percobaan login pada sistem

### Poin D.1
jika diberikan command "dl" maka sistem akan mendownload gambar acak dari https://loremflickr.com/320/240 sejumlah yang diinput oleh user
    
    printf "Enter command [dl or att]: "
    read command
    elif [[ $command == dl ]]
    then
    func_dl_pic
    
di cek command apakah yang diinput oleh user jika dl maka akan menjalankan func_dl_pic

    func_dl_pic(){
    printf "Enter number: "
    read n

    if [[ ! -f "$folder.zip" ]]
    then
    mkdir $folder
    count=0
    func_start_dl
    else
    func_unzip
    fi
    
untuk mengecek berapa banyak gambar yang ingin didownload oleh user,setelah diinput lalu menjalankan func_start_dl

    func_start_dl(){
    for(( i=$count+1; i<=$n+$count; i++ ))
    do
    wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
    done

    zip --password $password -r $folder.zip $folder/
    rm -rf $folder
    }

setelah itu dieksekusi dowload ke https://loremflickr.com/320/240 sebanyak yang diinput oleh user dengan nama file PIC_urutanangka.jpg

### Poin D.2
jika diberikan command "att" maka sistem akan mengecek log untuk berapa user yang mencoba untuk login ke sistem

    printf "Enter command [dl or att]: "
    read command
    if [[ $command == att ]]
    then
    func_att

setelah di cek jika command "att" maka sistem akan langsung menjalankan func_att

    func_att(){
    if [[ ! -f "$locLog" ]]
    then
    echo "Log Not Found"
    else
    awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}'     $locLog
    fi
    }

setelah itu func_att akan mengecek dari log untuk berapa user yang mencoba login ke sistem


## Soal 2
Pada soal 2, ditugaskan untuk membaca log website https://daffainfo.info dan membuat sebuah script awk yang dapat melaksakan berbagai tugas. Hasil dari tugas tersebut disimpan dalam file bernama ratarata.txt untuk poin B dan result.txt untuk poin C, D, dan E.

### Poin A: Membuat folder forensic_log_website_daffainfo_log
Directory dapat dibuat dengan menggunakan command

    `mkdir forensic_log_website_daffainfo_log`
 
### Poin B: Menghitung rata-rata request per jam pada website
Langkah pertama yaitu menghitung setiap request per jam dimulai dari jam 00 hingga 12. Kemudian hasil request per jam tersebut disimpan dalam count untuk mentotal semua request per jam menggunakan loop. Setelah itu didapatkan count sebanyak 973 request dan didapatkan total sebanyak 13. Akan tetapi, diketahui bahwa request dimulai dari jam 00 hingga 12 maka hasil total yaitu 13 dikurangi 1, sehingga jika 973 dibagi dengan 12 jam didapatkan 81.0833 request per jam.

    `
    awk -F: '{if (NR==1) next 
    countRequest[$3]++}
    END{for (i in countRequest){ 
    count=countRequest[i]+count 
    total++}
    }
    END{fix=total-1
    print"Rata-rata serangan adalah sebanyak",count/fix,"request per jam\n"}
    `
 
Selanjutnya hasil perhitungan tersebut disimpan dalam file ratarata.txt menggunakan

    `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt`
 
### Poin C: Menampilkan IP yang paling banyak melakukan request ke server dan jumlah requestnya
Langkah pertama yaitu menghitung IP dengan request terbanyak yang kemudian disimpan dalam max. Setelah itu ditampilkan IP dengan request terbanyak berserta jumlah requestnya dengan loop. Didapatkan IP 216.93.144.47 mengakses paling banyak ke server dengan jumlah request total 539.

    `
    awk -F: 'NR>1{ gsub(/"/, "")}
    {if (IP[$1]++>=max) 
	max=IP[$1]}
    END {for (i in IP) 
	if (max==IP[i]) 
	print"IP yang paling banyak mengakses server adalah:",i,"sebanyak",IP[i],"requests\n"}
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

 `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`
 
### Poin D: Menampilkan jumlah request dengan user-agent curl
Karena kata curl spesifik dan berbeda dengan yang lain, maka dapat dicari menggunakan command /curl/ dan jika menemukan maka akan ditambah terus menerus. Sehingga didapatkan sebanyak 58 request yang menggunakan curl sebagai user-agent.

    `
    awk -F: ' /curl/ {count++}
    END{print"Ada",count,"request yang menggunakan curl sebagai user-agent\n"}
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

 `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`
 
### Poin E: Menampilkan IP yang mengakses pada jam 2 pagi
Karena yang dicari adalah jam maka dapat dispesifikkan yang dicari pada kolom 3 dan difilter 02 karena jam 2 pagi. Sehingga dapat ditampilkan bahwa IP 128.14.133.58, 109.237.103.38, 88.80.186.144, 2.228.115.154, 145.239.154.84, 194.126.224.140.

    `
    awk -F: 'NR>1{ gsub(/"/, "")}
    {if($3~/02/) IP[$1]++}
    END {for(i in IP) print "IP",i,"mengakses pada jam 2 pagi"}
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

 `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`
 
### Kendala
Selama pengerjaan soal 2 terdapat beberapa kendala, salah satunya yaitu masih ada beberapa syntax yang cukup asing. Ketika mengerjakan soal 2b kurang fokus, hasil total menjadi 13 karena dihitung dari 0-12. Sedangkan waktu berjalannya request hanya 12 jam, sehingga yang seharusnya dibagi 12 menjadi dibagi 13 dan menyebabkan kesalahan pada hasil akhir soal 2b.

## Soal 3
Pada soal ini, kita akan membuat dua shell script dengan nama minute_log.sh dan aggregate_minutes_to_hourly_log.sh. Script minute_log.sh berfungsi untuk mengambil data dari RAM dan Disk untuk dimasukkan ke dalam file log dengan format metrics_{YmdHms}.log.Sedangkan untuk file aggregate_minutes_to_hourly_log.sh berfungsi untuk mengambil data-data yang dibuat tiap menitnya untuk diolah dan ditampilkan nilai maksimum, minimum, dan rata-rata untuk tiap jenis data yang sama untuk dimasukkan ke dalam file log dengan format metrics_agg_{YmdH}.log.

### minute_log.sh
Pertama-tama, kita akan membuat sebuah directory bernama log dengan command 

    ```mkdir log```

diikuti dengan mengambil data resource RAM dan memasukkan data tersebut ke dalam file sementara dengan command 

    ```free -m /home/dioz > temp.txt```

Setelah itu, kita akan print stream file sementara tersebut ke dalam dua file, yaitu file text1 
dan file text2 sementara yang berfungsi untuk mengakumulasi setiap data agar dapat memudahkan kita dalam mengambil data yang diperlukan dengan command 

    ```cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' >> text1.txt
    cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' > text2.txt```

File text1 dibuat untuk memudahkan kita dalam menyusun data seperti format yang harus diikuti.

Setelah itu, kita tambahkan nama path dengan menggunakan command 
    
    ```echo -n `pwd`"," >> text1.txt
    echo -n `pwd`"," >> text2.txt```

Untuk menambahkan resource Disk, kita dapat mengambilnya dengan command
    ```du -sh | awk '{print $1}' >> text1.txt
    du -sh | awk '{print $1}' >> text2.txt```

Karena untuk isi file text1.txt dan text2.txt sudah sesuai dengan format yang diminta, sekarang kita akan memasukkannya kedalam file metrics dengan command

    ```cat text1.txt >> log/metrics_"`date +"%Y%m%d%H%M%S"`".log
    cat text2.txt >> log/metrics_hourly_"`date +"%Y%m%d%H"`".log```

Jika kita coba untuk jalankan file tersebut, maka akan kelihatan hasil dari script minute_log.sh seperti pada gambar dibawah ini

![satu](soal3/screenshot/hasil_bash_minute_log.png)

Untuk isi tiap filenya akan seperti gambar dibawah

![dua](soal3/screenshot/isi_metrics_minute.png)

Setelah file metrics berhasil dibuat, maka kita atur permissionnya agar hanya bisa diakses oleh user saja dengan command

    ```chmod 700 log/metrics_"`date +"%Y%m%d%H%M%S"`".log
    chmod 700 log/metrics_hourly_"`date +"%Y%m%d%H"`".log```

### aggregate_minutes_to_hourly_log.sh
Untuk script ini, masukkan terlebih dahulu format isi file log ke dalam file metrics dengan command

    ```echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/metrics_agg_"`date +"%Y%m%d%H"`".log```

Dengan file hourly_log sementara yang telah dibuat sebelumnya, kita mengambil tiga nilai yang telah diolah yaitu minimum, maksimum, dan rata-rata dari tiap jenis data yang sama dan dimasukkan ke dalam file aggregate dengan format metrics_agg_{YmdH} dengan command sebagai berikut

#### Minimum
    ```echo -n "minimum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
    cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log | awk -F, '
    BEGIN {var1=15000; var2=15000; var3=15000; var4=15000; var5=15000; var6=15000; var7=15000; var8=15000; var9=15000}
    {if($1<var1) var1=$1;}
    {if($2<var2) var2=$2;}
    {if($3<var3) var3=$3;}
    {if($4<var4) var4=$4;}
    {if($5<var5) var5=$5;}
    {if($6<var6) var6=$6;}
    {if($7<var7) var7=$7;}
    {if($8<var8) var8=$8;}
    {if($9<var9) var9=$9;}
    END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log```

#### Maksimum
    ```echo -n "maximum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
    cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
    BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
    {if($1>var1) var1=$1;}
    {if($2>var2) var2=$2;}
    {if($3>var3) var3=$3;}
    {if($4>var4) var4=$4;}
    {if($5>var5) var5=$5;}
    {if($6>var6) var6=$6;}
    {if($7>var7) var7=$7;}
    {if($8>var8) var8=$8;}
    {if($9>var9) var9=$9;}
    END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log```

#### Rata-rata
    ```echo -n "average,"  >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
    cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
    BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
    {var1+=$1;}
    {var2+=$2;}
    {var3+=$3;}
    {var4+=$4;}
    {var5+=$5;}
    {var6+=$6;}
    {var7+=$7;}
    {var8+=$8;}
    {var9+=$9;}
    END {print var1/60","var2/60","var3/60","var4/60","var5/60","var6/60","var7/60","var8/60","var9/60}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log```

Ketika file tersebut dijalankan maka akan dikeluarkan output file log seperti gambar dibawah ini

![tiga](soal3/screenshot/hasil_bash_aggregate.png)

Untuk isi dari file metrics aggregate akan terlihat seperti ini (Lihat list file dengan nama metrics_agg_{YmdH} pada baris kedua dari bawah)

![empat](soal3/screenshot/isi_metrics_aggregate.png)

Setelah kita membuat dua script diatas, maka jangan lupa untuk melakukan instalasi task crontab dengan command cronjob

    ```* * * * * bash minute_log.sh
    59 * * * * bash aggregate_minutes_to_hourly_log.sh```

Untuk hasil dari crontab diatas dapat dilihat pada screenshot dibawah

![lima](soal3/screenshot/hasil_crontab.png)

Terakhir, sama seperti script minute_log.sh kita akan mengubah permission untuk file aggregasi sehingga hanya bisa diakses oleh user saja dengan command

    ```chmod 700 log/metrics_agg_"`date +"%Y%m%d%H"`".log```

### Kendala
Selama pengerjaan modul 1 kemarin kendala yang saya alami berkutat pada syntax asing pada awk command, appending data, dan menemukan cara mengaggregasi tiap file metrics menjadi satu file metrics untuk hourly.

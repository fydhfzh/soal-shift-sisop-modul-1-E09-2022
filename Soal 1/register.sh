#!/bin/bash
TIMESTAMP=`date "+%m-%d-%Y %H:%M:%S"`

echo "Username"

while :
do
read uname
if grep -Fxq "$uname" user2.txt
then
echo 'Username already exists'
echo '$TIMESTAMP REGISTER: ERROR User already exists' >> log.txt
else
break
fi
done

echo "Enter your password. (Your input will be hidden)"
echo "Note : Password must be at least 8 Alphanumeric characters long and minimum one uppercase and one lowercase letter, and different from your username."

while :
do
read -s pwd
[ ${#pwd} -ge 8 ] && [ $pwd != $uname ] && [[ "$pwd" =~ [[:upper:]] ]] && [[ "$pwd" =~ [[:lower:]] ]] && [[ $pwd =~ ^[a-zA-Z0-9]{3,20}$ ]] && break
echo "Your password didn't meet the requirements, try again!"
done

echo "Register success!"
echo "$TIMESTAMP REGISTER: INFO User $uname registered successfully" >> log.txt

echo "$uname" >> user2.txt
echo "$pwd" >> user2.txt



#!/bin/bash
TIMESTAMP=`date "+%m-%d-%Y %H:%M:%S"`



func_check_pass(){
    lengthPassword=${#password}

    if [[ $lengthPassword -lt 8 ]]
        then
            echo "Password must be more than 8 characters"

    elif [[ "$password" =~ [[:upper:]] && "$password" =~ [[:lower:]] && "$password" =~ ^[a-zA-z0-9 ]]
     then
              echo "Password must have at least an upper,lower,or number characters!"
        else
        func_login 
    fi
}

func_login(){
    checkUser=$(egrep $username "$locUser")
    checkPass=$(egrep $password "$locUser")
 


	
        if [[ -n "$checkUser" ]] && [[ -n "$checkPass" ]]
        then
            echo "$calendar $time LOGIN:INFO User $username logged in" >> $locLog
            echo "Login success"

            printf "Enter command [dl or att]: "
            read command
            if [[ $command == att ]]
            then
                func_att
            elif [[ $command == dl ]]
            then
                func_dl_pic
            else
                echo "Not found"
            fi

        else
            fail="Failed login attempt on user $username"
            echo $fail

            echo "$calendar $time LOGIN:ERROR $fail" >> $locLog
        fi
    
}

func_dl_pic(){
    printf "Enter number: "
    read n

    if [[ ! -f "$folder.zip" ]]
    then
        mkdir $folder
        count=0
        func_start_dl
    else
        func_unzip
    fi

}

func_unzip(){
    unzip -P $password $folder.zip
    rm $folder.zip

    count=$(find $folder -type f | wc -l)
    func_start_dl
}

func_start_dl(){
    for(( i=$count+1; i<=$n+$count; i++ ))
    do
        wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
    done

    zip --password $password -r $folder.zip $folder/
    rm -rf $folder
}

func_att(){
    if [[ ! -f "$locLog" ]]
    then
        echo "Log Not Found"
    else
        awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' $locLog
    fi
}


# main
calendar=$(date +%D)
time=$(date +%T)

printf "Enter Your Username: "
read username

printf "Enter Your Password: "
read -s password

# deff dir
folder=$(date +%Y-%m-%d)_$username
locLog=~/log.txt
locUser=~/user2.txt

#call fun check password
func_check_pass

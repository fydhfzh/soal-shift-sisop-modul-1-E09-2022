#!/bin/bash
mkdir log

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/metrics_agg_"`date +"%Y%m%d%H"`".log

#Minimum
echo -n "minimum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log | awk -F, '
BEGIN {var1=15000; var2=15000; var3=15000; var4=15000; var5=15000; var6=15000; var7=15000; var8=15000; var9=15000}
{if($1<var1) var1=$1;}
{if($2<var2) var2=$2;}
{if($3<var3) var3=$3;}
{if($4<var4) var4=$4;}
{if($5<var5) var5=$5;}
{if($6<var6) var6=$6;}
{if($7<var7) var7=$7;}
{if($8<var8) var8=$8;}
{if($9<var9) var9=$9;}
END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log


#Maximum
echo -n "maximum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
{if($1>var1) var1=$1;}
{if($2>var2) var2=$2;}
{if($3>var3) var3=$3;}
{if($4>var4) var4=$4;}
{if($5>var5) var5=$5;}
{if($6>var6) var6=$6;}
{if($7>var7) var7=$7;}
{if($8>var8) var8=$8;}
{if($9>var9) var9=$9;}
END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log


#Average
echo -n "average,"  >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
{var1+=$1;}
{var2+=$2;}
{var3+=$3;}
{var4+=$4;}
{var5+=$5;}
{var6+=$6;}
{var7+=$7;}
{var8+=$8;}
{var9+=$9;}
END {print var1/60","var2/60","var3/60","var4/60","var5/60","var6/60","var7/60","var8/60","var9/60}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log

chmod 700 log/metrics_agg_"`date +"%Y%m%d%H"`".log
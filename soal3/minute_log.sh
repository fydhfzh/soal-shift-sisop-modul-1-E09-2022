#!/bin/bash
mkdir log

free -m /home/dioz > temp.txt
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > text1.txt
cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' >> text1.txt
cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' > text2.txt

cat temp.txt | awk '{ORS=""}FNR == 3 {print $2 "," $3 "," $4 ","}' >> text1.txt
cat temp.txt | awk '{ORS=""}FNR == 3 {print $2 "," $3 "," $4 ","}' >> text2.txt

echo -n `pwd`"," >> text1.txt
echo -n `pwd`"," >> text2.txt

du -sh | awk '{print $1}' >> text1.txt
du -sh | awk '{print $1}' >> text2.txt

cat text1.txt >> log/metrics_"`date +"%Y%m%d%H%M%S"`".log
cat text2.txt >> log/metrics_hourly_"`date +"%Y%m%d%H"`".log

chmod 700 log/metrics_"`date +"%Y%m%d%H%M%S"`".log
chmod 700 log/metrics_hourly_"`date +"%Y%m%d%H"`".log


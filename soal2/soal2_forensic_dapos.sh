mkdir forensic_log_website_daffainfo_log

awk -F: '{if (NR==1) next 
        countRequest[$3]++}
END{for (i in countRequest){ 
        count=countRequest[i]+count 
        total++}
}
END{fix=total-1
print"Rata-rata serangan adalah sebanyak",count/fix,"request per jam\n"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

awk -F: 'NR>1{ gsub(/"/, "")}
{if (IP[$1]++>=max) 
	max=IP[$1]}
END {for (i in IP) 
	if (max==IP[i]) 
	print"IP yang paling banyak mengakses server adalah:",i,"sebanyak",IP[i],"requests\n"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk -F: ' /curl/ {count++}
END{print"Ada",count,"request yang menggunakan curl sebagai user-agent\n"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk -F: 'NR>1{ gsub(/"/, "")}
{if($3~/02/) IP[$1]++}
END {for(i in IP) print "IP",i,"mengakses pada jam 2 pagi"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

